package com.company;

public class Televisao {
    protected int canal, volume;
    protected boolean tv;

    public Televisao() {
    }

    public boolean ligarTV() {
        tv = true;
        canal = 501;
        volume = 20;
        return tv;
    }

    public boolean desligarTV() {
        tv = false;
        return tv;
    }

    public int getCanal() {
        return canal;
    }

    public void setCanal(int canal) {
        if (canal <= 0) {
            System.out.println("Canal inválido!");
        } else {
            this.canal = canal;
        }
    }

    public int getVolume() {
        return volume;
    }

}
