package com.company;

import java.util.Scanner;

public class IO {

    public static int menu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("MENU CONTROLE REMOTO");
        System.out.println("1 para ligar a TV, 2 para desligar a TV");
        System.out.println("3 para subir um canal, 4 para descer um canal");
        System.out.println("5 para subir o volume, 6 para descer o volume");
        System.out.println("7 para informar o canal que deseja");
        System.out.println("8 para colocar/tirar a TV do mudo");
        System.out.println("9 para consultar o volume e o canal atual");
        System.out.println("10 para sair do programa");
        System.out.println();
        int menu = scanner.nextInt();
        return menu;
    }

    public static int canal() {
        System.out.println("Digite o canal desejado:");
        Scanner scanner = new Scanner(System.in);
        int canal = scanner.nextInt();
        return canal;
    }

    public static void consulta(int canal, int volume) {
        System.out.println("Canal:" + canal + " no volume: " + volume);
    }
}
