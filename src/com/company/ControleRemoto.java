package com.company;

public class ControleRemoto extends Televisao {
    private int sobeCanal, desceCanal, sobeVolume, desceVolume;

    public ControleRemoto() {
        sobeCanal = +1;
        desceCanal = -1;
        sobeVolume = +1;
        desceVolume = -1;
    }

    public void menu() {
        int opcao;
        do {
            opcao = IO.menu();
            if (opcao == 1) {
                if (tv) {
                    System.out.println("TV já está ligada");
                } else {
                    ligarTV();
                    System.out.println("TV ligada");
                }
                ligarTV();
            } else if (opcao == 2) {
                if (!tv) {
                    System.out.println("TV já está desligada");
                } else {
                    desligarTV();
                    System.out.println("TV desligada");
                }
            } else if (opcao == 3) {
                if (!tv) {
                    System.out.println("TV desligada");
                } else {
                    canal += sobeCanal;
                }
            } else if (opcao == 4) {
                if (!tv) {
                    System.out.println("TV desligada");
                } else if (canal == 1) {
                    System.out.println("Já está no primeiro canal");
                } else {
                    canal += desceCanal;
                }
            } else if (opcao == 5) {
                if (!tv) {
                    System.out.println("TV desligada");
                } else {
                    volume += sobeVolume;
                }
            } else if (opcao == 6) {
                if (!tv) {
                    System.out.println("TV desligada");
                }  else if (volume == 0) {
                    System.out.println("Já está no mínimo!");
                } else {
                    volume += desceVolume;
                }
            } else if (opcao == 7) {
                if (!tv) {
                    System.out.println("TV desligada");
                } else {
                    setCanal(IO.canal());
                }
            } else if (opcao == 8) {
                if (!tv) {
                    System.out.println("TV desligada");
                } else {
                    if (volume == 0) {
                        volume = 20;
                        System.out.println("TV saiu do mudo. Volume: " + volume);
                    } else {
                        volume = 0;
                        System.out.println("TV no mudo");
                    }
                }
            } else if (opcao == 9) {
                if (!tv) {
                    System.out.println("TV desligada");
                } else {
                    IO.consulta(getCanal(), getVolume());
                }
            } else if (opcao == 10) {
                if (tv) {
                    System.out.println("Voce é socio da eletropaulo? Vai deixar a TV ligada mesmo?");
                }
            } else {
                System.out.println("Opção Inválida!");
            }
        } while (opcao != 10 || tv);
    }

    public int getSobeCanal() {
        return sobeCanal;
    }

    public int getDesceCanal() {
        return desceCanal;
    }

    public int getSobeVolume() {
        return sobeVolume;
    }

    public int getDesceVolume() {
        return desceVolume;
    }
}

